Planung - Vorwissenaktivierung/PräInstruktion. 
(Jeder für sich, schriftlich.  Vermutungen sind auch gut)


1.) Ganz generell: Wie kann man eine grosse Aufgabe planen?


2.) Welche Planungsobjekte/-elemente sind zu planen?


3.) Wenn mehrere Personen beteiligt sind, wie kann man als 
    Projektleiter alle beteiligen?


4.) Was kann man mit der Critical Path Method CPM herausfinden?


5.) Warum macht man ein Sitzungsprotokoll?
