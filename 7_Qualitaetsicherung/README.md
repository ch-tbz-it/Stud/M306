# M306/7 Qualitätssicherung

**_(32 % Modulnote)_**


### Aufgabenstellung als Prüfungssituation für 3-4 Lektionen

**Themen/Lernziele:** 
 - Projektmanagement, 
 - Zusammenarbeiten im (Projekt-)Team, 
 - Qualitätssicherung 
 - Review

1.) Stellen Sie ein Team mit 3-5 (ideal 4) Personen zusammen. Der/die Projektleiter:in teilt der Lehrperson bis in 20 Minuten (per TEAMS-Chat) mit, wer dabei ist und welche Rolle jede:r hat.


2.) Lesen Sie die 2 Theorie-Dokumente und richten Sie Ihre Arbeiten am Projekt möglichst nach den gelesenen Empfehlungen aus.
- [Warum Qualitätssicherung](./M306_7_Qualitaetssicherung.pdf)
- [Qualitaetsicherung und Reviewtechnik](./M306_7_Qualitaetsicherung_Reviewtechnik.pdf)

3.) Lesen Sie die Aufgabe und bearbeiten Sie die Aufgabe nach Vorgabe der Aufgabenstellung. Beachten Sie, dass Sie **30 min** vor Modul-Halbtag-Abschluss fertig sein müssen um ein Review des Projektdokumentation eines anderen Teams machen zu können 
--> (Euer Dokument muss dem anderen Team zur Verfügung stehen / Deren Dokument muss euch zur Verfügung stehen). LOGISCH, gell!

_Der **REVIEW** (des Projektdokumets des anderen Teams) kommt schlussendlich **in euer Dokument** als letztes Kapitel hinein (--> referenzieren Sie, welches Team Sie reviewt haben)._

