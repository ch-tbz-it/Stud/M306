# M306/5 Projekt-Kontrolle und Vorgangsmodelle

- [Projektkontrolle_Praeinstruktion](./M306_5_Projektkontrolle_Praeinstruktion.txt)


## Theorie
- [Skript (Seiten 34-40)](../docs) 
- Theorie/Skript [Projektkontrolle (alternativ)](./M306_5_Projektkontrolle.pdf)
- Übersicht [Vorgehensmodelle](./01_Vorgehensmodelle-Sarre2015.pdf) von F.Sarre
- [HERMES_2013.pdf](./02_HERMES_2013.pdf) als Vorgehensmodell der Bundesverwaltung [hermes.admin.ch](https://www.hermes.admin.ch),
<br> [hermes.zh.ch](http://hermes.zh.ch), Kt. ZH [Szenario-Übersicht.pdf](http://hermes.zh.ch/szenarien/szenario_01_IT-Individualanwendung/scenariooverview/de/Szenariouebersicht.pdf), [Vorlagen](http://hermes.zh.ch/szenarien/szenario_01_IT-Individualanwendung/templates/de/index.html)
<br> [Hermes Kleinprojekte Kt. ZH](http://ec2-34-242-70-192.eu-west-1.compute.amazonaws.com/anwenderloesung/szenarien-overview.xhtml)

- [What is SCRUM (scrum.org)](https://www.scrum.org/resources/what-is-scrum)
- (7:51 min, E) [Introduction to SCRUM in 7 Minutes](https://www.youtube.com/watch?v=9TycLR0TqFA)

## Aufgabe
- **Einzelarbeit**: [Ausarbeitung/Vergleich von Vorgehensmodellen (in Selbstinstruktion)](./M306_5_Vorgehensmodelle_AusarbeitungSelbstinstruktion.pdf)

