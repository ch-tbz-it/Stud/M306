# M306/1 Projektziele

## Einstieg
- [Praeinstruktion "Projekt", "Ziel"](./M306_1_Projekt-Ziele_Praeinstruktion.txt)
- 05:20 min [Die Vasa, der Stolz der schwedischen Marine](https://www.youtube.com/watch?v=ObIvBgkEGW4&t=15s)

## Theorie
- [Skript (Seiten 4-15)](../docs) 


- 04:40 min [01 Was ist ein Projekt?](https://www.youtube.com/watch?v=xEb__ktVJwc&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO)
- 06:35 min [02 Was ist ein Projektziel, was nicht?](https://www.youtube.com/watch?v=P5bHdkxLNZU&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=3)


## Auftrag 
- a) [M306_01_Projektziele_Aufgabe_KuhlRecords.pdf](../uebungen/M306_1_Projektziele_Aufgabe_KuhlRecords.pdf)
- b) Planen einer Geburtstags-Party

[../vorlagen_beispiele/M306_Vorlage_Projektauftrag.docx](../vorlagen_beispiele/M306_Vorlage_Projektauftrag.docx)
