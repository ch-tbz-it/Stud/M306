# M306/Abschlussprojekt

_(33 % Modulnote)_


### Aufgabenstellung als Prüfungssituation für 4 oder 7 Lektionen 

**Themen/Lernziele:** 
 - Projektmanagement, 
 - Zusammenarbeiten im (Projekt-)Team, 
 - Rückblick auf alle Themen des Moduls

**Teamgrösse**
- 3-4 Personen

**Aufgabenstellung**
- Variante A: Abschlussprojekt als Erarbeitung eines Übersichtsdokuments [(PDF)](./M306_9_AbschlussprojektA_und_Dokumentenbewertung.pdf) / [(DOCX)](./M306_9_AbschlussprojektA_und_Dokumentenbewertung.docx)
- Variante B: [Organisation eines Triathlons](./M306_9_AbschlussprojektB_OrganisationEinesTriathlons.pdf) oder etwas in dieser Art (selbstbestimmt)

- [Bewertungsblatt / Dokumentenbewertung](./M306_9_AbschlussprojektA_und_Dokumentenbewertung.pdf)
